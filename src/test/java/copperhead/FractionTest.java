package copperhead;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class FractionTest {

    @Test
    public void fractionShouldBeCorrectlyCreated() throws NumberFormatException {
        assertThat(new Fraction(1, 2).toMixedString(), is("1/2"));
        assertThat(new Fraction(3, 2).toMixedString(), is("1 1/2"));
        assertThat(new TestFraction("1 1/2").denominator(), is(2));
        assertThat(new TestFraction("1 1/2").numerator(), is(3));
        assertThat(new Fraction(new Double("5.25")).toMixedString(), is("5 1/4"));
        assertThat(new Fraction(5.125D).toMixedString(), is("5 1/8"));
        assertThat(new Fraction(new Fraction(2, 1)).toMixedString(), is("2"));
    }

    @Test(expected = NumberFormatException.class)
    public void shouldThrowNumberFormatException() {
        new Fraction("abc");
    }

    @Test
    public void toStringTest() {
        assertThat(new Fraction("1 1/2").toString(), is("3/2"));
    }

    @Test
    public void subtractTest() {
        Fraction f1 = new Fraction("3 1/2");
        Fraction f2 = new Fraction("1/2");
        assertThat(f1.subtract(f2).toMixedString(), is("3"));

        Fraction f3 = new Fraction(3.0D);
        assertThat(f1.subtract(f3).toMixedString(), is("1/2"));
    }

    @Test
    public void addTest() {
        Fraction f1 = new Fraction("3");
        Fraction f2 = new Fraction("1 1/2");
        assertThat(f1.add(f2).toMixedString(), is("4 1/2"));
    }

    @Test
    public void multiplyTest() {
        Fraction f1 = new Fraction(1.5D);
        Fraction f2 = new Fraction(3.0D);
        assertThat(f1.multiply(f2).toMixedString(), is("4 1/2"));
    }

    @Test
    public void divideTest() {
        Fraction f1 = new Fraction("3/2");
        Fraction f2 = new Fraction("3/8");
        assertThat(f1.divide(f2).toMixedString(), is("4"));
    }

    @Test
    public void reciprocalTest() {
        assertThat(new Fraction("1/8").reciprocal().toMixedString(), is("8"));
    }

    @Test
    public void negateTest() {
        assertThat(new Fraction("-1/32").negated().toMixedString(), is("1/32"));
        assertThat(new Fraction("1/32").negated().toMixedString(), is("-1/32"));
    }

    @Test
    public void reducedTest() {
        assertThat(new Fraction("4/8").reduced().toString(), is("1/2"));
        assertThat(new Fraction("12/32").toMixedString(), is("3/8"));
    }

    @Test
    public void compareToTest() {
        Fraction f1 = new Fraction("3/8");
        Fraction f2 = new Fraction("1/2");
        assertThat(f1.compareTo(f2), is(-1));
        assertThat(f2.compareTo(f2), is(0));
        assertThat(f2.compareTo(f1), is(1));
    }

    @Test
    public void lessThanTest() {
        Fraction f1 = new Fraction("3/8");
        Fraction f2 = new Fraction("1/2");
        assertThat(f1.lessThan(f2), is(true));
        assertThat(f2.lessThan(f2), is(false));
        assertThat(f2.lessThan(f1), is(false));
    }

    @Test
    public void greaterThanTest() {
        Fraction f1 = new Fraction("3/8");
        Fraction f2 = new Fraction("1/2");
        assertThat(f1.greaterThan(f2), is(false));
        assertThat(f2.greaterThan(f2), is(false));
        assertThat(f2.greaterThan(f1), is(true));
    }

    @Test
    public void equalTest() {
        Fraction f1 = new Fraction("3/8");
        Fraction f2 = new Fraction("3/8");
        Fraction f3 = new Fraction("5/8");
        assertThat(f1.negated().equal(new Fraction("-3/8")), is(true));
        assertThat(f1.equal(f2), is(true));
        assertThat(f2.equal(f3), is(false));
    }

    @Test
    public void asFractionShouldReturnAFraction() {
        TestFraction f = new TestFraction(Fraction.asFraction(3));
        assertThat(f.numerator(), is(3));
        assertThat(f.denominator(), is(1));
        assertThat(Fraction.asFraction(new Fraction(3,8)).toMixedString(), is("3/8"));
    }

    class TestFraction extends Fraction {

        public TestFraction(String frac) {
            super(frac);
        }
        public TestFraction(Fraction frac) { super(frac); }

        public int denominator() {
            return _denominator;
        }

        public int numerator() {
            return _numerator;
        }

    }
}
