package copperhead;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Dim3DTest {
    @Test
    public void shouldOutputAsStringCorrectly() {
        final Dim3D dim3D = new Dim3D(5,4,3);
        final Dim3D dim3D2 = new Dim3D(new Fraction("24 1/8"), new Fraction("36 1/4"), new Fraction("36"));
        assertThat(dim3D.toString(), is("4x5x3"));
        assertThat(dim3D2.toString(), is("36 1/4x24 1/8x36"));
    }
}
