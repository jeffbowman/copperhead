package copperhead;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BaseCabinetTest {
    @Before
    public void setup() {
        bc = new BaseCabinet(new Dim3D(new Fraction("24"), new Fraction("60"), new Fraction("35 1/4")));
    }

    @Test
    public void baseDimTest() {
        assertThat(bc.baseDim().toString(), is("1 1/2x21 1/2"));
    }

    @Test
    public void bottomRailTest() {
        assertThat(bc.bottomRail().toString(), is("21x1 1/2"));
    }

    private BaseCabinet bc;
}
