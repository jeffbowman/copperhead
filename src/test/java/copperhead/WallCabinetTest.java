package copperhead;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class WallCabinetTest {
    @Before
    public void setup() {
        wc = new WallCabinet(new Dim3D(new Fraction(60), new Fraction(12), new Fraction("35 1/4")));
    }

    @Test
    public void openingTest() {
        assertThat(wc.opening().toString(), is("57x27 1/2"));
    }

    private WallCabinet wc;
}
