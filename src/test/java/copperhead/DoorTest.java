package copperhead;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DoorTest {
    @Test
    public void computeRailsCorrectly() {
        final Door door = new Door(new Dim3D(3,4,5));
        final Dim rails = door.rails();
        assertThat(rails.length().toMixedString(), is("1 5/16"));
        assertThat(rails.width().toMixedString(), is("2 3/8"));
    }
}
