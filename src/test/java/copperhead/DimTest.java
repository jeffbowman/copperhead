package copperhead;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DimTest {
    @Test
    public void shouldOutputAsStringCorrectly() {
        final Dim dim = new Dim(5,4);
        final Dim dim2 = new Dim(new Fraction("24 1/8"), new Fraction("36 1/4"));
        assertThat(dim.toString(), is("4x5"));
        assertThat(dim2.toString(), is("36 1/4x24 1/8"));

    }
}
