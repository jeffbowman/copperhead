package copperhead;

import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Pageable;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.ArrayList;
import java.util.Iterator;

public class PrintLayout implements Pageable, Printable {
    private ArrayList _lines;
    private ArrayList _pages;
    private int _x;
    private int _y;
    private int _imageableX;
    private int _imageableY;
    private PageFormat _pageFormat;
    private int _numPages;
    private Page _currentPage;
    private int _currentPageNumber;
    private boolean _finalized;

    public PrintLayout(PageFormat var1) {
        this._pageFormat = var1;
        this._pages = null;
        this._currentPage = null;
        this._currentPageNumber = 0;
        this._finalized = false;
    }

    public PrintLayout() {
        this._pageFormat = null;
        this._pages = null;
        this._currentPage = null;
        this._currentPageNumber = 0;
        this._finalized = false;
    }

    public void addLine(Line var1) {
        if (this._pageFormat != null) {
            if (this._pages == null) {
                this._pages = new ArrayList();
            }

            if (this._currentPage == null) {
                this._currentPage = new Page((int) this._pageFormat.getImageableWidth(), (int) this._pageFormat.getImageableHeight());
                ++this._currentPageNumber;
            }

            if (!this._currentPage.addLine(var1)) {
                this._pages.add(this._currentPage);
                this._currentPage = new Page((int) this._pageFormat.getImageableWidth(), (int) this._pageFormat.getImageableHeight());
                ++this._currentPageNumber;
                this._currentPage.addLine(var1);
            }
        } else {
            this._lines.add(var1);
        }

    }

    public void setPageFormat(PageFormat var1) {
        this._pageFormat = var1;
        this.addLinesToPage();
    }

    public int getNumberOfPages() {
        return this._currentPageNumber > this._pages.size() ? this._currentPageNumber : this._pages.size();
    }

    public PageFormat getPageFormat(int var1) throws IndexOutOfBoundsException {
        return this._pageFormat;
    }

    public Printable getPrintable(int var1) throws IndexOutOfBoundsException {
        return (Page) this._pages.get(var1);
    }

    public int print(Graphics var1, PageFormat var2, int var3) {
        int var4 = 1;

        if (var3 < this._pages.size()) {
            var4 = ((Page) this._pages.get(var3)).print(var1, this._pageFormat, var3);
        }

        return var4;
    }

    public void finalize() throws Exception {
        if (!this._finalized) {
            if (this._pageFormat == null) {
                throw new Exception("PageFormat not provided");
            } else {
                if (this._currentPageNumber > this._pages.size()) {
                    this._pages.add(this._currentPage);
                }

                System.out.println("total pages: " + this._pages.size());
                this._finalized = true;
            }
        }
    }

    private void addLinesToPage() {
        if (this._lines.size() > 0) {
            if (this._currentPage == null) {
                this._currentPage = new Page((int) this._pageFormat.getImageableWidth(), (int) this._pageFormat.getImageableHeight());
                ++this._currentPageNumber;
            }

            Iterator var1 = this._lines.iterator();

            while (var1.hasNext()) {
                Line var2 = (Line) var1.next();
                if (!this._currentPage.addLine(var2)) {
                    if (this._pages == null) {
                        this._pages = new ArrayList();
                    }

                    this._pages.add(this._currentPage);
                    this._currentPage = new Page((int) this._pageFormat.getImageableWidth(), (int) this._pageFormat.getImageableHeight());
                    ++this._currentPageNumber;
                    this._currentPage.addLine(var2);
                }
            }
        }

        for (int var3 = 0; var3 < this._lines.size(); ++var3) {
            this._lines.remove(var3);
        }

    }

}
