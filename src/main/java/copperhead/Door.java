package copperhead;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Door extends AbstractCabinet implements Cabinet {

    public Door(Dim3D dim3D) {
        _dimension = dim3D;
    }

    public Dim bulkhead() {
        return null;
    }

    public Dim back() {
        final Fraction len = _dimension.length();
        final Fraction width = _dimension.width();
        final Dim backDimension = new Dim(len.subtract(new Fraction(3, 1)),
                width.subtract(new Fraction(3, 1)));
        return backDimension;
    }

    public Collection<Map<String, String>> fields() {
        List<Map<String, String>> fieldList = new ArrayList<>();
        HashMap<String, String> typeMap = new HashMap<>();
        HashMap<String, String> dimsMap = new HashMap<>();
        String dim2D = new Dim(_dimension.length(), _dimension.width()).toString(); // only want length x width
        typeMap.put("Type: ", "DOOR " + id());
        dimsMap.put("Dims: ", dim2D);
        fieldList.add(typeMap);
        fieldList.add(dimsMap);
        fieldList.add(showStiles());
        fieldList.add(showRails());
        fieldList.add(showBack());
        return fieldList;
    }

    public Dim mullions() {
        return null;
    }

    public int numRails() {
        return 2;
    }

    public int numStiles() {
        return 2;
    }

    public Dim rails() {
        Fraction width = _dimension.width();
        return new Dim(width.subtract(new Fraction("2 11/16")), new Fraction("2 3/8"));
    }

    public Dim stiles() {
        Fraction length = _dimension.length();
        return new Dim(length.add(new Fraction("1 1/16")), new Fraction("2 3/8"));
    }

    public Dim opening() {
        return null;
    }

    public String toString() {
        final Dim dim2D = new Dim(_dimension.length(), _dimension.width());
        return id() + ":DOOR: " + dim2D.toString();
    }

    public Dim shelves() {
        return null;
    }

    public Map showBulkheads() {
        return null;
    }

    public Map<String, String> showDims() {
        Map<String, String> dimsMap = new HashMap<>();
        dimsMap.put("Dims: ", dimension().toString());
        return dimsMap;
    }

    public Map<String, String> showMullions() {
        return null;
    }

    public Map<String, String> showOpening() {
        return null;
    }

    public Map<String, String> showShelves() {
        return null;
    }

    public Map<String, String> showBack() {
        Map<String, String> backMap = new HashMap<>();
        backMap.put("Panel: ", back().toString());
        return backMap;
    }

    public Map<String, String> showStiles() {
        Map<String, String> stilesMap = new HashMap();
        stilesMap.put("Stiles: ", numStiles() + " @ " + stiles().toString());
        return stilesMap;
    }

    public String id() {
        return super.id();
    }

    public void id(String var1) {
        super.id(var1);
    }

}
