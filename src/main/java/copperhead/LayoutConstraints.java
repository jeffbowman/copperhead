package copperhead;

import java.awt.*;
import java.awt.print.PageFormat;

public class LayoutConstraints {
    public int anchor;
    public int fill;
    public Font font;
    public int xpad;
    public int ypad;
    public int maxwidth;
    public int imageablewidth;
    public int linewidth;
    public boolean wrap;
    public boolean underline;
    public static final int WEST = 0;
    public static final int EAST = 1;
    public static final int NONE = 0;
    public static final int CENTERED = 1;
    public static final int FLUSH = 2;
    public static final int PAGEWIDTH = -1;

    public LayoutConstraints() {
        this.anchor = this.fill = this.xpad = this.ypad = 0;
        this.underline = this.wrap = false;
        this.linewidth = -1;
        this.font = new Font("SansSerif", 0, 10);
    }

    public LayoutConstraints(PageFormat var1) {
        this.anchor = this.fill = this.xpad = this.ypad = 0;
        this.underline = this.wrap = false;
        this.linewidth = -1;
        this.font = new Font("SansSerif", 0, 10);
        this.imageablewidth = (int) var1.getImageableWidth();
    }

    public LayoutConstraints(int var1) {
        this.anchor = this.fill = this.xpad = this.ypad = 0;
        this.underline = this.wrap = false;
        this.linewidth = -1;
        this.font = new Font("SansSerif", 0, 10);
        this.imageablewidth = var1;
    }

    public LayoutConstraints(int var1, int var2) {
        this.anchor = var1;
        this.fill = var2;
        this.xpad = this.ypad = 0;
        this.underline = this.wrap = false;
        this.linewidth = -1;
        this.font = new Font("SansSerif", 0, 10);
    }

    public LayoutConstraints(int var1, int var2, Font var3) {
        this.anchor = var1;
        this.fill = var2;
        this.font = var3;
        this.xpad = this.ypad = 0;
        this.underline = this.wrap = false;
        this.linewidth = -1;
    }

    public LayoutConstraints(int var1, int var2, Font var3, int var4, int var5) {
        this.anchor = var1;
        this.fill = var2;
        this.xpad = var4;
        this.ypad = var5;
        this.font = var3;
        this.underline = this.wrap = false;
        this.linewidth = -1;
    }

}
