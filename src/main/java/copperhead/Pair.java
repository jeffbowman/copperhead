package copperhead;

public class Pair<L, R> {
    private L left;
    private R right;

    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public Pair() {
        this.left = null;
        this.right = null;
    }

    public void left(L left) {
        this.left = left;
    }

    public L left() {
        return this.left;
    }

    public void right(R right) {
        this.right = right;
    }

    public R right() {
        return this.right;
    }

    public String toString() {
        return "(" + this.left.toString() + ", " + this.right.toString() + ")";
    }

}
