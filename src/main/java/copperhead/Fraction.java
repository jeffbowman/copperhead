package copperhead;

public class Fraction extends Number implements Comparable {
    public static final Fraction one = new Fraction(1);
    public static final Fraction two = new Fraction(2);
    public static final Fraction twenty = new Fraction(20);

    public Fraction() {
        this._numerator = 0;
        this._denominator = 1;
    }

    public Fraction(int numerator, int demonimator) {
        this._numerator = numerator;
        this._denominator = demonimator;
    }

    public Fraction(Fraction fraction) {
        this._numerator = fraction._numerator;
        this._denominator = fraction._denominator;
    }

    public Fraction(String fraction) throws NumberFormatException {
        String fracString = fraction.trim();
        int spaceIndex = fracString.indexOf(' ');
        int slashIndex = fracString.indexOf('/');
        if (spaceIndex != -1 && slashIndex == -1) {
            throw new NumberFormatException("Invalid Fraction '" + fracString + "'");
        } else {
            int wholeNumber = -1;
            if (spaceIndex != -1) {
                wholeNumber = new Integer(fracString.substring(0, spaceIndex));
            }

            boolean var6 = true;
            boolean var7 = true;
            int numerator;
            int denominator;
            if (slashIndex != -1) {
                numerator = new Integer(fracString.substring(spaceIndex + 1, slashIndex));
                denominator = new Integer(fracString.substring(slashIndex + 1));
            } else {
                numerator = new Integer(fracString.substring(spaceIndex + 1));
                denominator = 1;
            }

            if (spaceIndex != -1) {
                this._numerator = wholeNumber * denominator + numerator;
            } else {
                this._numerator = numerator;
            }

            this._denominator = denominator;
        }
    }

    public Fraction(double number) {
        Fraction fraction = (new Fraction((int) (number * 32.0D), 32)).reduced();
        this._numerator = fraction._numerator;
        this._denominator = fraction._denominator;
    }

    public Fraction(Double number) {
        this(number.doubleValue());
    }

    public Fraction add(Fraction var1) {
        int var3 = this.gcd(this._denominator, var1._denominator);
        int var4 = var1._denominator / var3;
        int var5 = this._denominator / var3;
        int var2 = this._numerator * var4 + var1._numerator * var5;
        var4 *= var5;
        var5 = this.gcd(var2, var3);
        var2 /= var5;
        var3 = var4 * (var3 / var5);
        return new Fraction(var2, var3);
    }

    public int compareTo(Object other) throws ClassCastException {
        if (!(other instanceof Fraction)) {
            throw new ClassCastException("can't compare " + other.toString() + " as Fraction");
        } else {
            Fraction otherFraction = (Fraction) other;
            if (this.equal(otherFraction)) {
                return 0;
            } else {
                return this.lessThan(otherFraction) ? -1 : 1;
            }
        }
    }

    public Fraction divide(Fraction var1) {
        return new Fraction(this.multiply(var1.reciprocal()));
    }

    public double doubleValue() {
        return this.toFloat().doubleValue();
    }

    public boolean equal(Fraction var1) {
        if (this._numerator == 0) {
            return var1._numerator == 0;
        } else {
            int var2 = this._numerator * var1._denominator;
            int var3 = this._denominator * var1._numerator;
            return var2 == var3;
        }
    }

    public float floatValue() {
        return this.toFloat();
    }

    public int gcd(int var1, int var2) {
        int var3 = var1;

        int var10000;
        int var4;
        for (var4 = var2; var3 != 0; var3 = var10000 % var3) {
            var10000 = var4;
            var4 = var3;
        }

        return Math.abs(var4);
    }

    public boolean greaterThan(Fraction var1) {
        int var2 = this._numerator * var1._denominator;
        int var3 = this._denominator * var1._numerator;
        return var2 > var3;
    }

    public int intValue() {
        return this.truncated();
    }

    public boolean lessThan(Fraction var1) {
        int var2 = this._numerator * var1._denominator;
        int var3 = this._denominator * var1._numerator;
        return var2 < var3;
    }

    public long longValue() {
        return new Long((long) this.truncated());
    }

    public Fraction multiply(Fraction var1) {
        int var2 = this.gcd(this._numerator, var1._denominator);
        int var3 = this.gcd(this._denominator, var1._numerator);
        Fraction var4 = null;
        if (var3 == this._denominator && var2 == var1._denominator) {
            var4 = new Fraction(this._numerator / var2 * (var1._numerator / var3), 1);
        } else {
            var4 = new Fraction(this._numerator / var2 * (var1._numerator / var3), this._denominator / var3 * (var1._denominator / var2));
        }

        return var4;
    }

    public Fraction negated() {
        return new Fraction(this._numerator * -1, this._denominator);
    }

    public Fraction reciprocal() throws Error {
        int var1 = this._numerator;
        int var2 = this._denominator;
        Fraction var3 = null;
        if (var1 == 0) {
            throw new Error("0 has no reciprocal");
        } else {
            if (var1 == -1) {
                new Fraction(var2 * -1, var1 * -1);
            }

            var3 = new Fraction(var2, var1);
            return var3;
        }
    }

    public Fraction reduced() {
        int var1 = this.gcd(this._numerator, this._denominator);
        int var2 = this._numerator / var1;
        int var3 = this._denominator / var1;
        return new Fraction(var2, var3);
    }

    public Fraction subtract(Fraction var1) {
        return new Fraction(this.add(var1.negated()));
    }

    public Float toFloat() {
        float var1 = (float) this._numerator / (float) this._denominator;
        return new Float(var1);
    }

    public String toString() {
        if (this._denominator == 1) {
            return "" + this._numerator;
        } else {
            Fraction var1 = new Fraction(this._numerator, this._denominator).reduced();
            return "" + var1._numerator + "/" + var1._denominator;
        }
    }

    public String toMixedString() {
        if (this._denominator == 1) {
            return "" + this._numerator;
        } else {
            final Fraction reduced = new Fraction(this._numerator, this._denominator).reduced();
            int truncated = 0;
            if (reduced._numerator < 0) {
                // if the numerator is negative, use ceil insted of floor
                // as we have to invert the direction toward zero with negative
                // numbers
                truncated = new Double(Math.ceil((double) this._numerator / (double) this._denominator)).intValue();
            } else {
                truncated = reduced.truncated();
            }
            StringBuilder builder = new StringBuilder();
            final int partial = reduced._numerator % reduced._denominator;
            boolean isMixedFraction = false;
            if (truncated != 0) {
                builder.append(truncated);
                isMixedFraction = true;
            }

            if (partial != 0) {
                builder.append((isMixedFraction ? " " : ""))
                        .append(partial)
                        .append("/")
                        .append(reduced._denominator);
            }

            return builder.toString();
        }
    }

    public static Fraction asFraction(Number number) {
        if (number instanceof Fraction) {
            return new Fraction((Fraction) number);
        }

        return new Fraction(number.intValue(), 1);
    }

    protected int truncated() {
        return (new Double(Math.floor((double) this._numerator / (double) this._denominator))).intValue();
    }

    protected int _numerator;
    protected int _denominator;

}
