package copperhead;

import java.util.*;

public class WallCabinet extends AbstractCabinet implements Cabinet {
    public WallCabinet(Dim3D dimension) {
        _dimension = dimension;
    }

    public Dim bulkhead() {
        Fraction fraction = _dimension.width().subtract(new Fraction(3, 4));
        return new Dim(fraction, _dimension.height());
    }

    public Dim back() {
        Fraction length = _dimension.length().subtract(new Fraction(1, 2));
        Fraction width = _dimension.height().subtract(new Fraction(1, 1));
        return new Dim(length, width);
    }

    public Collection<Map<String, String>> fields() {
        List<Map<String, String>> fieldList = new ArrayList<>();
        Map<String, String> typeMap = new HashMap<>();
        Map<String, String> dimsMap = new HashMap<>();
        typeMap.put("Type: ", "WALL " + id());
        dimsMap.put("Dims: ", _dimension.toString());
        fieldList.add(typeMap);
        fieldList.add(dimsMap);
        fieldList.add(showStiles());
        fieldList.add(showRails());
        fieldList.add(showMullions());
        fieldList.add(showOpening());
        fieldList.add(showShelves());
        fieldList.add(showBulkheads());
        fieldList.add(showBack());
        return fieldList;
    }

    public Dim mullions() {
        Fraction mullionWidth = _dimension.height().subtract(rails().length().multiply(Fraction.two));
        return new Dim(Fraction.two, mullionWidth);
    }

    public int numRails() {
        return 2;
    }

    public Dim opening() {
        Fraction length = null;
        Fraction stilesLen = stiles().length();
        Fraction stilesCount = new Fraction(numStiles());
        Fraction stilesWidth = _dimension.length().subtract(stilesCount.multiply(stilesLen));
        if (stilesWidth.greaterThan(Fraction.twenty)) {
            Fraction mullionCount = new Fraction(numMullions());
            Fraction halfMullions = mullionCount.divide(Fraction.two);
            length = stilesWidth.subtract(mullionCount)
                    .divide(halfMullions)
                    .add(Fraction.one);
        } else {
            length = stilesWidth;
            stilesWidth = _dimension.height().subtract(rails().length().multiply(Fraction.two));
        }

        return new Dim(length, stilesWidth);
    }

    public String toString() {
        return id() + ":WALL: " + _dimension.toString();
    }

    public Dim shelves() {
        Number var1 = _dimension.length();
        Fraction var2 = var1 instanceof Fraction ? (Fraction) var1 : new Fraction(var1.toString());
        var2 = var2.subtract(new Fraction(1, 1));
        var1 = _dimension.width();
        Fraction var3 = var1 instanceof Fraction ? (Fraction) var1 : new Fraction(var1.toString());
        var3 = var3.subtract(new Fraction(1, 1));
        Dim var4 = new Dim(var2, var3);
        return var4;
    }

    public Map<String, String> showBulkheads() {
        Map<String, String> var1 = new HashMap<>();
        var1.put("Bulkhead: ", bulkhead().toString());
        return var1;
    }

    public Map<String, String> showDims() {
        Map<String, String> var1 = new HashMap<>();
        var1.put("Dims: ", dimension().toString());
        return var1;
    }

    public Map<String, String> showMullions() {
        Map<String, String> var1 = new HashMap<>();
        String var2 = "" + numMullions() + " @ " + mullions().toString();
        var1.put("Mullions: ", var2);
        return var1;
    }

    public Map<String, String> showOpening() {
        Map<String, String> var1 = new HashMap<>();
        int var2 = numMullions() + 1;
        var1.put("Opening: ", "" + var2 + "  @ " + opening().toString());
        return var1;
    }

    public Map<String, String> showRails() {
        Map<String, String> var1 = new HashMap<>();
        int var2 = numRails();
        var1.put("Rails: ", "" + var2 + " @ " + rails().toString());
        return var1;
    }

    public Map<String, String> showShelves() {
        Map<String, String> var1 = new HashMap<>();
        var1.put("Shelves: ", shelves().toString());
        return var1;
    }

    public Map<String, String> showStiles() {
        Map<String, String> var1 = new HashMap<>();
        int var2 = numStiles();
        var1.put("Stiles: ", "" + var2 + " @ " + stiles().toString());
        return var1;
    }

    public Dim stiles() {
        Fraction var1 = new Fraction(3, 2);
        Number var2 = _dimension.length();
        Fraction var3 = var2 instanceof Fraction ? (Fraction) var2 : new Fraction(var2.toString());
        Dim var4 = new Dim(var1, var3);
        return var4;
    }

    public String id() {
        return super.id();
    }

    public void id(String var1) {
        super.id(var1);
    }

    private Map<String, String> showBack() {
        Map<String, String> var1 = new HashMap<>();
        var1.put("Back: ", back().toString());
        return var1;
    }
}
