package copperhead;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractCabinet implements Cabinet {
    public AbstractCabinet() {
    }

    public Dim3D dimension() {
        if (this._dimension == null) {
            this._dimension = new Dim3D();
        }

        return this._dimension;
    }

    public void dimension(Dim3D dim3D) {
        this._dimension = dim3D;
    }

    public int numMullions() {
        Number tempLength = this.dimension().length();
        Fraction dimLength = null;
        dimLength = tempLength instanceof Fraction ? (Fraction) tempLength : new Fraction(tempLength.toString());
        tempLength = this.stiles().length();
        Fraction stilesLength = tempLength instanceof Fraction ? (Fraction) tempLength : new Fraction(tempLength.toString());
        Fraction numMullions = dimLength.subtract(stilesLength.multiply(new Fraction(this.numStiles(), 1)));
        numMullions = numMullions.divide(new Fraction(20, 1));
        return numMullions.truncated() * 2;
    }

    public int numStiles() {
        return 2;
    }

    public Dim rails() {
        Fraction two = new Fraction(2, 1);
        Number tempLength = this.dimension().length();
        Fraction var3 = tempLength instanceof Fraction ? (Fraction) tempLength : new Fraction(tempLength.toString());
        tempLength = this.stiles().length();
        Fraction stilesLength = tempLength instanceof Fraction ? (Fraction) tempLength : new Fraction(tempLength.toString());
        Fraction var5 = two.subtract(stilesLength.multiply(new Fraction(this.numStiles(), 1)));
        Fraction railsLength = var3.subtract(var5);
        return new Dim(two, railsLength);
    }

    public Map<String, String> showDims() {
        Map<String, String> dimsMap = new HashMap<>();
        dimsMap.put("Dims: ", this.dimension().toString());
        return dimsMap;
    }

    public Map<String, String> showRails() {
        Map<String, String> railsMap = new HashMap<>();
        railsMap.put("Rails: ", (new Integer(this.numRails())).toString() + " @ " + this.rails().toString());
        return railsMap;
    }

    public String getLenStr() {
        return _dimension.length().toMixedString();
    }

    public String getWidStr() {
        return _dimension.width().toMixedString();
    }

    public String getHgtStr() {
        return _dimension.height().toMixedString();
    }

    public String id() {
        return this._id;
    }

    public void id(String var1) {
        this._id = var1;
    }

    protected Dim3D _dimension;
    protected String _id;
}
