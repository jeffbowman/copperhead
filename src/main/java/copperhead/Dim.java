package copperhead;

public class Dim {
    public Dim() {
        this._length = new Fraction("0/1");
        this._width = new Fraction("0/1");
    }

    public Dim(Number length, Number width) {
        _length = Fraction.asFraction(length);
        _width = Fraction.asFraction(width);
    }

    public Fraction length() {
        return this._length;
    }

    public Fraction width() {
        return this._width;
    }

    public String toString() {
        if (str != null && str.length() > 0 && str.contains("x")) {
            return str;
        }

        StringBuilder builder = new StringBuilder(_width.toMixedString());
        builder.append("x")
                .append(_length.toMixedString());
        str = builder.toString();

        return str;
    }

    protected Fraction _length;
    protected Fraction _width;

    private String str;
}
