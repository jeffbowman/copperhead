package copperhead;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.lang.reflect.Method;
import java.util.List;
import java.util.*;

public class LineTemplate {
    private List<Pair<Integer, Pair<Integer, Column>>> _items;
    private HashMap _groups;
    private Font _font;
    private int _columnNumber;
    private int _columnWidth;
    private int _pageWidth;
    private LayoutConstraints _constraints;

    public LineTemplate(Font var1, int var2) {
        this._items = new ArrayList<>();
        this._groups = new HashMap();
        this._font = var1;
        this._columnNumber = 0;
        this._columnWidth = 0;
        this._pageWidth = var2;
        this._constraints = new LayoutConstraints();
    }

    public LineTemplate(Font var1, int var2, LayoutConstraints var3) {
        this(var1, var2);
        this._constraints = var3;
    }

    public void add(String var1) {
        ++this._columnNumber;
        Column column = new Column();
        column.unadjustedSize = this.getTextLength(var1);
        column.columnSize = column.unadjustedSize;
        Pair<Integer, Column> var3 = new Pair<>(-1, column);
        Pair<Integer, Pair<Integer, Column>> var4 = new Pair<>(this._columnNumber, var3);
        this._items.add(var4);
        if (this._constraints.fill == 2) {
            this._columnWidth = this._pageWidth / this._columnNumber;
            this.recalcWidth();
        }
    }

    public void add(String line, int var2, int var3) {
        ++this._columnNumber;
        Column column = new Column();
        column.unadjustedSize = this.getTextLength(line);
        column.columnSize = column.unadjustedSize;
        this._items.add(new Pair<Integer, Pair<Integer, Column>>(this._columnNumber, new Pair<>(var2, column)));
        this.addToGroup(var2, var3, line);
        this.recalcWidth();
    }

    public void add(String var1, int var2) {
        this.add(var1, var2, -1);
    }

    public List<Pair<Integer, Pair<Integer, Column>>> getColumns() {
        return this._items;
    }

    public LayoutConstraints getConstraints() {
        return this._constraints;
    }

    public int getPad() {
        return this._constraints.xpad;
    }

    public int getPad(int var1, int var2) {
        int var3 = this._constraints.xpad;
        if (this._groups.containsKey(new Integer(var1))) {
            LineTemplate.Group var4 = (LineTemplate.Group) this._groups.get(new Integer(var1));
            if (var2 > var4.numElements) {
                return var3;
            }

            var3 = var4.groupPad;
        }

        return var3;
    }

    private int getTextLength(String var1) {
        BufferedImage var2 = new BufferedImage(640, 480, 10);
        FontMetrics var3 = var2.createGraphics().getFontMetrics(this._font);
        return var3.stringWidth(var1);
    }

    private void recalcWidth() {
        Set var1 = this._groups.keySet();
        int var2 = 0;
        Iterator var3 = var1.iterator();
        boolean var4 = false;
        Pair var7;
        int var8;
        Iterator var10;
        Pair var12;
        if (var1.size() <= 0) {
            for (var10 = this._items.iterator(); var10.hasNext(); var2 += var8) {
                var12 = (Pair) var10.next();
                var7 = (Pair) var12.right();
                var8 = ((LineTemplate.Column) var7.right()).columnSize;
            }
        } else {
            label36:
            while (true) {
                if (!var3.hasNext()) {
                    var10 = this._items.iterator();

                    while (true) {
                        if (!var10.hasNext()) {
                            break label36;
                        }

                        var12 = (Pair) var10.next();
                        var7 = (Pair) var12.right();
                        var8 = (Integer) var7.left();
                        int var9 = ((LineTemplate.Column) var7.right()).columnSize;
                        if (var8 == -1) {
                            var2 += var9;
                        }
                    }
                }

                Integer var5 = (Integer) var3.next();
                LineTemplate.Group var6 = (LineTemplate.Group) this._groups.get(var5);
                var2 += var6.groupSize;
            }
        }

        int var11 = this._pageWidth - var2;
        if (var1.size() <= 1) {
            this._columnWidth = var11 / this._items.size();
        } else {
            this._columnWidth = var11 / var1.size();
        }

        this.reassignWidth();
    }

    private boolean useSet() {
        ListIterator var1 = this._items.listIterator();
        Method[] var2 = null;
        boolean var3 = true;

        try {
            var2 = var1.getClass().getMethods();
        } catch (SecurityException var5) {
            var3 = false;
        }

        for (int var4 = 0; var4 < var2.length; ++var4) {
            if (var2[var4].getName().equals("set")) {
                var3 = true;
                break;
            }
        }

        return var3;
    }

    private void reassignWidth() {
        ListIterator var1 = this._items.listIterator();
        Pair var2 = (Pair) this._items.get(0);
        Integer var3 = (Integer) ((Pair) var2.right()).left();
        boolean var4 = false;
        Integer var5 = null;
        Pair var6 = null;
        if (this.useSet()) {
            while (var1.hasNext()) {
                var6 = (Pair) var1.next();
                var5 = (Integer) ((Pair) var6.right()).left();
                if (var4) {
                    var3 = (Integer) ((Pair) var6.right()).left();
                }

                LineTemplate.Column var12;
                if (var5.compareTo(new Integer(-1)) == 0) {
                    var12 = (LineTemplate.Column) ((Pair) var6.right()).right();
                    var12.columnSize = var12.unadjustedSize + this._columnWidth;
                    ((Pair) var6.right()).right(var12);
                    var1.set(var6);
                    var4 = true;
                }

                if (var3.compareTo(new Integer(-1)) != 0 && var5.compareTo(var3) != 0) {
                    var3 = (Integer) ((Pair) var6.right()).left();
                    var4 = false;
                    var6 = (Pair) var1.previous();
                    var6 = (Pair) var1.previous();
                    var1.next();
                    var12 = (LineTemplate.Column) ((Pair) var6.right()).right();
                    var12.columnSize = var12.unadjustedSize + this._columnWidth;
                    ((Pair) var6.right()).right(var12);
                    var1.set(var6);
                }
            }
        } else {
            ArrayList var7 = new ArrayList();
            int var8 = this._items.size();

            for (int var9 = 0; var9 < var8; ++var9) {
                var6 = (Pair) this._items.get(var9);
                var5 = (Integer) ((Pair) var6.right()).left();
                if (var4) {
                    var3 = (Integer) ((Pair) var6.right()).left();
                }

                if (var5.compareTo(new Integer(-1)) == 0) {
                    LineTemplate.Column var10 = (LineTemplate.Column) ((Pair) var6.right()).right();
                    var10.columnSize = var10.unadjustedSize + this._columnWidth;
                    ((Pair) var6.right()).right(var10);
                    var7.add(var6);
                    var4 = true;
                }

                if (var3.compareTo(new Integer(-1)) != 0 && var5.compareTo(var3) != 0) {
                    var3 = (Integer) ((Pair) var6.right()).left();
                    var4 = false;
                    int var13 = var9 - 1;
                    var6 = (Pair) this._items.get(var13);
                    LineTemplate.Column var11 = (LineTemplate.Column) ((Pair) var6.right()).right();
                    var11.columnSize = var11.unadjustedSize + this._columnWidth;
                    ((Pair) var6.right()).right(var11);
                    var7.add(var6);
                }
            }
        }

    }

    private void addToGroup(int var1, int var2, String var3) {
        Integer var4 = new Integer(var1);
        int var5 = 0;
        int var6 = this.getTextLength(var3);
        LineTemplate.Group var7;
        if (this._groups.containsKey(var4)) {
            var7 = (LineTemplate.Group) this._groups.get(var4);
            if (var7.groupPad != var2 && var2 != -1) {
                var7.groupPad = var2;
            }

            var7.unadjustedSizes.add(new Integer(var6));
            var7.numElements = var7.unadjustedSizes.size();

            int var9;
            for (Iterator var8 = var7.unadjustedSizes.iterator(); var8.hasNext(); var5 += var9) {
                var9 = (Integer) var8.next();
            }

            var5 += var7.groupPad * (var7.numElements - 1);
            var7.groupSize = var5;
            this._groups.put(var4, var7);
        } else {
            this.getTextLength(var3);
            var7 = new LineTemplate.Group();
            var7.unadjustedSizes = new ArrayList();
            if (var2 != -1) {
                var7.groupPad = var2;
            }

            var7.unadjustedSizes.add(new Integer(var6));
            var7.numElements = var7.unadjustedSizes.size();
            var7.groupSize = var6 + var7.groupPad * (var7.numElements - 1);
            this._groups.put(var4, var7);
        }

    }

    class Column {
        public int unadjustedSize;
        public int columnSize;

        Column() {
        }

        public String toString() {
            return "Column " + this.unadjustedSize + " " + this.columnSize;
        }
    }

    class Group {
        public ArrayList unadjustedSizes;
        public int groupPad;
        public int groupSize;
        public int numElements;

        Group() {
        }

        public String toString() {
            return "Group " + this.numElements + " " + this.groupSize + " " + this.groupPad;
        }
    }

}
