package copperhead;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class Copperhead extends JFrame {
    private JList _cabinetList;
    private JLabel _projectTitle;
    private JLabel _heightLabel;
    private JComboBox _cabinetTypes;
    private JTextField _length;
    private JTextField _width;
    private JTextField _height;
    private JTextField _id;
    private JTextArea _cutlist;
    private JTextArea _aggregateCutlist;
    private JButton _add;
    private JButton _update;
    private JButton _delete;
    private JButton _save;
    private JButton _load;
    private JButton _printProject;
    private JButton _printCabinet;
    private JButton _changeTitle;
    private JButton _exit;
    private Vector _cabinets;
    private boolean _listSelectionChanging;
    private HashMap _aggregate;
    private PageFormat _format;
    private Vector _printLines;
    private Font _font;
    private int _linesPerPage;
    private int _baseLine;
    private int _lineSpaceing;

    public Copperhead() {
        String[] var1 = new String[]{"BASE", "WALL", "DOOR"};
        DefaultListModel var2 = new DefaultListModel();
        this._cabinetList = new JList(var2);
        this._projectTitle = new JLabel("No title");
        this._cabinetTypes = new JComboBox(var1);
        this._length = new JTextField(8);
        this._width = new JTextField(8);
        this._height = new JTextField(8);
        this._id = new JTextField(8);
        this._heightLabel = new JLabel("Height: ");
        this._cutlist = new JTextArea(8, 20);
        this._aggregateCutlist = new JTextArea(8, 20);
        this._aggregate = new HashMap();
        this._listSelectionChanging = false;
        this._cabinetList.setSelectionMode(0);
        this.setupListSelectionHandler();
        this.setupTypeSelectionHandler();
        this._cutlist.setEditable(false);
        this._aggregateCutlist.setEditable(false);
        this._projectTitle.setForeground(Color.black);
        this._width.setText("24");
        this._height.setText("35 1/4");
        this._add = new JButton(this.addAction());
        this._update = new JButton(this.updateAction());
        this._delete = new JButton(this.deleteAction());
        this._save = new JButton(this.saveAction());
        this._load = new JButton(this.loadAction());
        this._printProject = new JButton(this.printAggregate());
        this._printCabinet = new JButton(this.printCabinet());
        this._changeTitle = new JButton(this.changeTitleAction());
        this._exit = new JButton(this.exitAction());
        GridBagConstraints var3 = new GridBagConstraints();
        var3.gridx = var3.gridy = 0;
        var3.anchor = GridBagConstraints.EAST;
        var3.ipadx = 5;
        var3.fill = GridBagConstraints.BOTH;
        this.getContentPane().setLayout(new GridBagLayout());
        var3.gridx = var3.gridy = 0;
        this.getContentPane().add(this.setupTitle(), var3);
        var3.gridy = GridBagConstraints.RELATIVE;
        // var3.weightx = var3.weighty = 1.0;
        this.getContentPane().add(this.setupCabinet(), var3);
        this.getContentPane().add(this.setupCutlist(), new GridBagConstraints(0, -1, 1,1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 1,1));
        var3.anchor = GridBagConstraints.WEST;
        this.getContentPane().add(this.setupButtonRow(), var3);
        this.setTitle("Copperhead Cutlist");
        this.enableButtons();
        this._font = new Font("Monospaced", 0, 10);
        this._lineSpaceing = 11;
        this._linesPerPage = -1;
        this._baseLine = -1;
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent var1) {
                System.exit(0);
            }
        });
    }

    public int getNumberOfPages() {
        if (this._linesPerPage == -1) {
            this._linesPerPage = (int) Math.floor(this._format.getImageableHeight() / (double) this._lineSpaceing);
        }

        return this._printLines != null ? (this._printLines.size() - 1) / this._linesPerPage + 1 : 0;
    }

    public void print() {
        PageFormat var1 = new PageFormat();
        Paper var2 = new Paper();
        var2.setImageableArea(35.0D, 35.0D, var2.getWidth() - 72.0D, var2.getHeight() - 72.0D);
        var1.setPaper(var2);
        PrintLayout var3 = new PrintLayout(var1);
        LayoutConstraints var4 = new LayoutConstraints();
        Iterator var5 = this._printLines.iterator();

        while (var5.hasNext()) {
            var3.addLine(new Line((String) var5.next(), var4));
        }

        try {
            var3.finalize();
        } catch (Exception var9) {
            var9.printStackTrace();
        }

        PrinterJob var6 = PrinterJob.getPrinterJob();
        var6.setPageable(var3);

        try {
            if (var6.printDialog()) {
                var6.print();
            }
        } catch (Exception var8) {
            var8.printStackTrace();
        }

    }

    public static void main(String[] var0) {
        Copperhead copperhead = new Copperhead();
        copperhead.pack();
        copperhead.setVisible(true);
    }

    private JPanel setupTitle() {
        JPanel var1 = new JPanel();
        var1.add(new JLabel("Project Title: "));
        var1.add(this._projectTitle);
        var1.add(this._changeTitle);
        var1.setBorder(new EtchedBorder());
        return var1;
    }

    private JPanel setupCabinet() {
        GridBagConstraints var1 = new GridBagConstraints();
        var1.gridx = var1.gridy = 0;
        var1.anchor = 13;
        var1.fill = 1;
        var1.ipadx = 5;
        JPanel var2 = new JPanel();
        var2.setLayout(new GridBagLayout());
        JScrollPane var3 = new JScrollPane(this._cabinetList);
        var3.setPreferredSize(new Dimension(253, 100));
        var2.add(var3, new GridBagConstraints(0,0, 1,1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 2, 0));
        var1.gridx = 1;
        var1.gridy = 0;
        var1.anchor = 13;
        var1.fill = 1;
        var2.add(this.setupDim(), var1);
        var1.gridx = 2;
        var2.add(this.setupButtons(), var1);
        var2.setBorder(new EtchedBorder());
        return var2;
    }

    private JPanel setupDim() {
        GridBagConstraints var1 = new GridBagConstraints();
        var1.gridx = var1.gridy = 0;
        var1.anchor = 13;
        var1.ipadx = 5;
        var1.fill = 2;
        JPanel var2 = new JPanel();
        var2.setLayout(new GridBagLayout());
        var2.setBorder(new EtchedBorder());
        var2.add(new JLabel("Type: "), var1);
        ++var1.gridx;
        var2.add(this._cabinetTypes, var1);
        var1.gridx = 0;
        var1.gridy = 1;
        var2.add(new JLabel("Width: "), var1);
        ++var1.gridx;
        var2.add(this._width, var1);
        var1.gridx = 0;
        var1.gridy = 2;
        var2.add(new JLabel("Length: "), var1);
        ++var1.gridx;
        var2.add(this._length, var1);
        var1.gridx = 0;
        var1.gridy = 3;
        var2.add(this._heightLabel, var1);
        ++var1.gridx;
        var2.add(this._height, var1);
        ++var1.gridx;
        var2.add(new JLabel("ID: "), var1);
        ++var1.gridx;
        var2.add(this._id, var1);
        var2.setBorder(new EtchedBorder());
        return var2;
    }

    private JPanel setupButtons() {
        JPanel var1 = new JPanel();
        var1.setLayout(new BoxLayout(var1, 1));
        var1.add(this._add);
        var1.add(this._update);
        var1.add(this._delete);
        var1.setBorder(new EtchedBorder());
        return var1;
    }

    private JPanel setupCutlist() {
        JPanel panel = new JPanel();
        // panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
        panel.setLayout(new GridBagLayout());
        panel.add(new JScrollPane(this._cutlist), new GridBagConstraints(0,0, 1,1, 1.0,1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 5,0));
        panel.add(new JScrollPane(this._aggregateCutlist), new GridBagConstraints(1,0, 1,1, 1.0,1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(0,0,0,0), 5,0));
        panel.setBorder(new EtchedBorder());
        return panel;
    }

    private JPanel setupButtonRow() {
        JPanel var1 = new JPanel();
        var1.setLayout(new BoxLayout(var1, 0));
        var1.add(this._save);
        var1.add(this._load);
        var1.add(this._printCabinet);
        var1.add(this._printProject);
        var1.add(this._exit);
        return var1;
    }

    private void setupListSelectionHandler() {
        this._cabinetList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent var1) {
                Copperhead.this._listSelectionChanging = true;
                Cabinet var2 = (Cabinet) Copperhead.this._cabinetList.getSelectedValue();
                if (var2 != null) {
                    if (var2 instanceof BaseCabinet) {
                        Copperhead.this._cabinetTypes.setSelectedItem("BASE");
                    } else if (var2 instanceof WallCabinet) {
                        Copperhead.this._cabinetTypes.setSelectedItem("WALL");
                    } else if (var2 instanceof Door) {
                        Copperhead.this._cabinetTypes.setSelectedItem("DOOR");
                    }

                    Copperhead.this._length.setText(var2.getLenStr());
                    Copperhead.this._width.setText(var2.getWidStr());
                    Copperhead.this._id.setText(var2.id());
                    if (var2 instanceof Door) {
                        Copperhead.this._height.setVisible(false);
                        Copperhead.this._heightLabel.setVisible(false);
                    } else {
                        Copperhead.this._height.setText(var2.getHgtStr());
                        Copperhead.this._heightLabel.setVisible(true);
                        Copperhead.this._height.setVisible(true);
                    }

                    Copperhead.this.updateCutlist(var2);
                }

                Copperhead.this.enableButtons();
                Copperhead.this._listSelectionChanging = false;
            }
        });
    }

    private void setupTypeSelectionHandler() {
        this._cabinetTypes.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent var1) {
                if (var1.getStateChange() == 1 && !Copperhead.this._listSelectionChanging) {
                    Copperhead.this._cabinetList.clearSelection();
                    if (Copperhead.this._cabinetTypes.getSelectedItem().equals("BASE")) {
                        Copperhead.this._length.setText("");
                        Copperhead.this._width.setText("24");
                        Copperhead.this._height.setVisible(true);
                        Copperhead.this._heightLabel.setVisible(true);
                        Copperhead.this._height.setText("35 1/4");
                    } else if (Copperhead.this._cabinetTypes.getSelectedItem().equals("WALL")) {
                        Copperhead.this._length.setText("");
                        Copperhead.this._width.setText("12");
                        Copperhead.this._height.setVisible(true);
                        Copperhead.this._heightLabel.setVisible(true);
                        Copperhead.this._height.setText("");
                    } else if (Copperhead.this._cabinetTypes.getSelectedItem().equals("DOOR")) {
                        Copperhead.this._length.setText("");
                        Copperhead.this._width.setText("");
                        Copperhead.this._heightLabel.setVisible(false);
                        Copperhead.this._height.setVisible(false);
                    }
                }

            }
        });
    }

    private AbstractAction addAction() {
        return new AbstractAction("Add") {
            public void actionPerformed(ActionEvent var1) {
                if (Copperhead.this._length.getText().length() != 0 && Copperhead.this._width.getText().length() != 0 && Copperhead.this._height.getText().length() != 0) {
                    Object var2 = null;
                    Fraction var3 = new Fraction(Copperhead.this._length.getText());
                    Fraction var4 = new Fraction(Copperhead.this._width.getText());
                    Fraction var5 = new Fraction(Copperhead.this._height.getText());
                    if (Copperhead.this._cabinetTypes.getSelectedItem().equals("BASE")) {
                        var2 = new BaseCabinet(new Dim3D(var3, var4, var5));
                    } else if (Copperhead.this._cabinetTypes.getSelectedItem().equals("WALL")) {
                        var2 = new WallCabinet(new Dim3D(var3, var4, var5));
                    } else if (Copperhead.this._cabinetTypes.getSelectedItem().equals("DOOR")) {
                        var2 = new Door(new Dim3D(var3, var4, new Fraction(0, 1)));
                    }

                    if (var2 != null) {
                        ((Cabinet) var2).id(Copperhead.this._id.getText());
                        if (Copperhead.this._cabinets == null) {
                            Copperhead.this._cabinets = new Vector();
                        }

                        Copperhead.this._cabinets.add(var2);
                        Copperhead.this._cabinetList.setListData(Copperhead.this._cabinets);
                        Copperhead.this._cabinetList.setSelectedValue(var2, true);
                        Copperhead.this.updateAggregateCutlist();
                    }

                }
            }
        };
    }

    private AbstractAction updateAction() {
        return new AbstractAction("Update") {
            public void actionPerformed(ActionEvent var1) {
                int var2 = Copperhead.this._cabinetList.getSelectedIndex();
                Object var3 = null;
                if (var2 != -1) {
                    Fraction var4 = new Fraction(Copperhead.this._length.getText());
                    Fraction var5 = new Fraction(Copperhead.this._width.getText());
                    Fraction var6 = new Fraction(Copperhead.this._height.getText());
                    if (Copperhead.this._cabinetTypes.getSelectedItem().equals("BASE")) {
                        var3 = new BaseCabinet(new Dim3D(var4, var5, var6));
                    } else if (Copperhead.this._cabinetTypes.getSelectedItem().equals("WALL")) {
                        var3 = new WallCabinet(new Dim3D(var4, var5, var6));
                    } else if (Copperhead.this._cabinetTypes.getSelectedItem().equals("DOOR")) {
                        var3 = new Door(new Dim3D(var4, var5, new Fraction(0, 1)));
                    }

                    ((Cabinet) var3).id(Copperhead.this._id.getText());
                    Copperhead.this._cabinets.set(var2, var3);
                    Copperhead.this._cabinetList.setListData(Copperhead.this._cabinets);
                    Copperhead.this._cabinetList.setSelectedValue(var3, true);
                    Copperhead.this.updateCutlist((Cabinet) var3);
                    Copperhead.this.updateAggregateCutlist();
                }

            }
        };
    }

    private AbstractAction deleteAction() {
        return new AbstractAction("Delete") {
            public void actionPerformed(ActionEvent var1) {
                if (Copperhead.this._cabinetList.getModel().getSize() > 0 && Copperhead.this._cabinetList.getSelectedIndex() != -1) {
                    int var2 = Copperhead.this._cabinetList.getSelectedIndex();
                    Copperhead.this._cabinets.removeElementAt(var2);
                    Copperhead.this._cabinetList.setListData(Copperhead.this._cabinets);
                    Copperhead.this.clearCutlist();
                    Copperhead.this.updateAggregateCutlist();
                }

            }
        };
    }

    private AbstractAction saveAction() {
        return new AbstractAction("Save") {
            public void actionPerformed(ActionEvent var1) {
                if (Copperhead.this._cabinetList.getModel().getSize() > 0 && Copperhead.this._projectTitle.getText().length() > 0) {
                    FileOutputStream var2 = null;

                    try {
                        var2 = new FileOutputStream(Copperhead.this._projectTitle.getText());
                        Enumeration var3 = Copperhead.this._cabinets.elements();

                        while (var3.hasMoreElements()) {
                            String var4 = var3.nextElement().toString() + "\n";
                            var2.write(var4.getBytes());
                        }
                    } catch (Exception var13) {
                        System.out.println("Failed to write the file");
                        var13.printStackTrace();
                    } finally {
                        try {
                            var2.close();
                        } catch (IOException var12) {
                            var12.printStackTrace();
                        }

                    }
                }

            }
        };
    }

    private AbstractAction loadAction() {
        return new AbstractAction("Load") {
            public void actionPerformed(ActionEvent var1) {
                JFileChooser var2 = new JFileChooser();
                int var3 = var2.showOpenDialog(Copperhead.this);
                if (var3 == 0) {
                    Copperhead.this.loadFile(var2.getSelectedFile());
                }

            }
        };
    }

    private AbstractAction exitAction() {
        return new AbstractAction("Exit") {
            public void actionPerformed(ActionEvent var1) {
                System.exit(0);
            }
        };
    }

    private AbstractAction changeTitleAction() {
        return new AbstractAction("Change Title") {
            public void actionPerformed(ActionEvent var1) {
                String var2 = JOptionPane.showInputDialog("New Project Name");
                Copperhead.this._projectTitle.setText(var2);
                Copperhead.this.pack();
            }
        };
    }

    private AbstractAction printCabinet() {
        return new AbstractAction("Print Cabinet") {
            public void actionPerformed(ActionEvent var1) {
                int var2 = Copperhead.this._cabinetList.getSelectedIndex();
                Cabinet var3 = (Cabinet) Copperhead.this._cabinets.elementAt(var2);
                Copperhead.this._printLines = Copperhead.this.printCabinetSetup(var3);
                Copperhead.this.print();
            }
        };
    }

    private AbstractAction printAggregate() {
        return new AbstractAction("Print Project") {
            public void actionPerformed(ActionEvent var1) {
                Copperhead.this._printLines = Copperhead.this.printAggregateSetup();
                Copperhead.this.print();
            }
        };
    }

    private void enableSave() {
        if (this._projectTitle.getText().equals("No title")) {
            this._save.setEnabled(false);
        } else if (this._cabinetList.getModel().getSize() == 0) {
            this._save.setEnabled(false);
        } else {
            this._save.setEnabled(true);
        }

    }

    private void enablePrintCutlist() {
        if (this._cabinetList.getModel().getSize() == 0) {
            this._printCabinet.setEnabled(false);
        } else {
            this._printCabinet.setEnabled(true);
        }

    }

    private void enablePrintAggregate() {
        if (this._projectTitle.getText().equals("No title")) {
            this._printProject.setEnabled(false);
        } else if (this._cabinetList.getModel().getSize() == 0) {
            this._printProject.setEnabled(false);
        } else {
            this._printProject.setEnabled(true);
        }

    }

    private void enableUpdate() {
        if (this._cabinetList.getModel().getSize() != 0 && this._cabinetList.getSelectedIndex() != -1) {
            this._update.setEnabled(true);
        } else {
            this._update.setEnabled(false);
        }

    }

    private void enableDelete() {
        if (this._cabinetList.getModel().getSize() != 0 && this._cabinetList.getSelectedIndex() != -1) {
            this._delete.setEnabled(true);
        } else {
            this._delete.setEnabled(false);
        }

    }

    private void enableButtons() {
        this.enableSave();
        this.enablePrintCutlist();
        this.enablePrintAggregate();
        this.enableUpdate();
        this.enableDelete();
    }

    private void loadFile(File var1) {
        String var2 = new String();
        this._projectTitle.setText(var1.getName());

        try {
            FileInputStream var3 = new FileInputStream(var1);
            int var4 = var3.available();

            for (byte[] var5 = new byte[var4]; var3.read(var5) != -1; var5 = new byte[var4]) {
                String var6 = new String(var5);
                var2 = var2 + var6;
            }
        } catch (IOException var7) {
            var7.printStackTrace();
        }

        this.loadCabinets(this.splitData(var2));
    }

    private Vector splitData(String var1) {
        StringTokenizer var2 = new StringTokenizer(var1, "\n");
        Vector var3 = new Vector();

        while (var2.hasMoreTokens()) {
            String var4 = (String) var2.nextElement();
            var3.add(var4);
        }

        return var3;
    }

    private void loadCabinets(Vector var1) {
        Enumeration var2 = var1.elements();
        if (this._cabinets == null) {
            this._cabinets = new Vector();
        } else {
            this._cabinets.clear();
        }

        while (var2.hasMoreElements()) {
            String var3 = (String) var2.nextElement();
            Cabinet var4 = this.createCabinetFromString(var3);
            this._cabinets.add(var4);
        }

        this._cabinetList.setListData(this._cabinets);
        this._cabinetList.setSelectedIndex(0);
        this.updateAggregateCutlist();
        this.pack();
    }

    private Cabinet createCabinetFromString(String var1) {
        StringTokenizer var2 = new StringTokenizer(var1, ":");
        String var3 = null;
        String var4 = null;
        String var5 = null;
        String var6 = null;
        String var7 = null;
        String var8 = null;
        if (var2.hasMoreTokens()) {
            var8 = (String) var2.nextElement();
        }

        if (var2.hasMoreTokens()) {
            var3 = (String) var2.nextElement();
        }

        if (var2.hasMoreTokens()) {
            var4 = (String) var2.nextElement();
        }

        var2 = new StringTokenizer(var4, "x");
        if (var2.hasMoreTokens()) {
            var5 = (String) var2.nextElement();
        }

        if (var2.hasMoreTokens()) {
            var6 = (String) var2.nextElement();
        }

        if (var2.hasMoreTokens()) {
            var7 = (String) var2.nextElement();
        }

        if (var3 != null && var5 != null && var6 != null) {
            Object var9 = null;
            if (var3.equals("BASE")) {
                var9 = new BaseCabinet(new Dim3D(new Fraction(var5), new Fraction(var6), new Fraction(var7)));
            }

            if (var3.equals("WALL")) {
                var9 = new WallCabinet(new Dim3D(new Fraction(var5), new Fraction(var6), new Fraction(var7)));
            }

            if (var3.equals("DOOR")) {
                var9 = new Door(new Dim3D(new Fraction(var5), new Fraction(var6), new Fraction(0, 1)));
            }

            ((Cabinet) var9).id(var8);
            return (Cabinet) var9;
        } else {
            return null;
        }
    }

    private void clearCutlist() {
        this._cutlist.setText("");
    }

    private void updateCutlist(Cabinet var1) {
        String var2 = new String();
        ArrayList var3 = (ArrayList) var1.fields();
        Iterator var4 = var3.iterator();

        while (var4.hasNext()) {
            HashMap var5 = (HashMap) var4.next();
            Set var6 = var5.keySet();

            String var8;
            for (Iterator var7 = var6.iterator(); var7.hasNext(); var2 = var2 + var8 + var5.get(var8) + "\n") {
                var8 = (String) var7.next();
            }
        }

        this._cutlist.setText(var2);
    }

    private void updateAggregateCutlist() {
        this.rebuildHash();
        TreeSet var1 = new TreeSet(this._aggregate.keySet());
        Iterator var2 = var1.iterator();

        String var3;
        String var6;
        for (var3 = new String(); var2.hasNext(); var3 = var3 + var6) {
            String var4 = (String) var2.next();
            String var5 = ((Copperhead.Pair) this._aggregate.get(var4)).right.toString();
            var6 = var5 + " " + var4 + " [" + ((Copperhead.Pair) this._aggregate.get(var4)).left + "]\n";
        }

        if (var3 != null) {
            this._aggregateCutlist.setText(var3);
        }

    }

    private void rebuildHash() {
        this._aggregate.clear();
        Iterator var1 = this._cabinets.iterator();

        while (var1.hasNext()) {
            Cabinet var2 = (Cabinet) var1.next();
            Collection var3 = var2.fields();
            Iterator var4 = var3.iterator();

            while (var4.hasNext()) {
                HashMap var5 = (HashMap) var4.next();
                Set var6 = var5.keySet();
                Iterator var7 = var6.iterator();
                new String();
                new String();

                while (var7.hasNext()) {
                    String var10 = (String) var7.next();
                    if (!var10.trim().equals("Type:") && !var10.trim().equals("Dims:")) {
                        String var11 = (String) var5.get(var10);
                        String var8;
                        String var9;
                        StringTokenizer var12;
                        if (var10.trim().equals("Mullions:")) {
                            var12 = new StringTokenizer(var11, "@");
                            var8 = var12.nextToken();
                            String var13 = var12.nextToken();
                            StringTokenizer var14 = new StringTokenizer(var13, "&");
                            if (var14.countTokens() > 1) {
                                var9 = var14.nextToken();
                                var10 = var10 + var9;
                                this.addToAggregateHash(var10, Integer.parseInt(var8.trim()), var2.id());
                                var8 = var14.nextToken();
                                var9 = "Mullions: " + var12.nextToken();
                                this.addToAggregateHash(var9, Integer.parseInt(var8.trim()), var2.id());
                            } else {
                                var9 = "Mullions: " + var13;
                                this.addToAggregateHash(var9, Integer.parseInt(var8.trim()), var2.id());
                            }
                        } else {
                            var12 = new StringTokenizer(var11, "@");
                            if (var12.countTokens() > 1) {
                                var8 = var12.nextToken();
                                var9 = var12.nextToken();
                            } else {
                                var8 = "1";
                                var9 = var11;
                            }

                            var10 = var10 + var9;
                            this.addToAggregateHash(var10, Integer.parseInt(var8.trim()), var2.id());
                        }
                    }
                }
            }
        }

    }

    private void addToAggregateHash(String var1, int var2, String var3) {
        Copperhead.Pair var4 = new Copperhead.Pair();
        var4.left = var3;
        var4.right = new Integer(var2);
        if (this._aggregate.containsKey(var1)) {
            var4 = (Copperhead.Pair) this._aggregate.get(var1);
            var4.left = var4.left + " " + var3;
            int var5 = var4.right + var2;
            var4.right = new Integer(var5);
        }

        this._aggregate.put(var1, var4);
    }

    private Vector printCabinetSetup(Cabinet var1) {
        new String();
        ArrayList var3 = (ArrayList) var1.fields();
        Iterator var4 = var3.iterator();
        Vector var5 = new Vector();

        while (var4.hasNext()) {
            HashMap var6 = (HashMap) var4.next();
            Set var7 = var6.keySet();
            Iterator var8 = var7.iterator();

            while (var8.hasNext()) {
                String var9 = (String) var8.next();
                String var2 = var9 + var6.get(var9);
                var5.add(var2);
            }
        }

        return var5;
    }

    private Vector printAggregateSetup() {
        Vector var1 = new Vector();
        this.rebuildHash();
        TreeSet var2 = new TreeSet(this._aggregate.keySet());
        Iterator var3 = var2.iterator();
        var1.add("Cutlist for project: " + this._projectTitle.getText());
        var1.add(new String("    "));
        var1.add(new String("    "));
        var1.add(new String("    "));

        while (var3.hasNext()) {
            String var4 = (String) var3.next();
            String var5 = ((Copperhead.Pair) this._aggregate.get(var4)).right.toString();
            String var6 = var5 + " " + var4 + " [" + ((Copperhead.Pair) this._aggregate.get(var4)).left + "]";
            var1.add(var6);
        }

        var1.add(new String("    "));
        var1.add(new String("    "));
        var1.add(new String("    "));
        return var1;
    }

    class Pair {
        public String left;
        public Integer right;

        Pair() {
        }
    }

}
