package copperhead;

import copperhead.LineTemplate.Column;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class Line {
    private int _lineLength;
    private List<Pair<String, LayoutConstraints>> _elements;
    private LayoutConstraints _constraints;
    private LineTemplate _lineTemplate;
    private int _group;

    public Line() {
        this._lineTemplate = null;
        this._constraints = new LayoutConstraints();
        this._group = 1;
        _elements = new ArrayList<>();
    }

    public Line(String line, LayoutConstraints layoutConstraints) {
        Pair<String, LayoutConstraints> pair = new Pair(line, layoutConstraints);
        _elements = new ArrayList<>();
        this._elements.add(pair);
        this._constraints = layoutConstraints;
        this._group = 1;
    }

    public Line(PageFormat pageFormat) {
        this._group = 1;
        this._lineLength = (int) pageFormat.getImageableWidth();
        this._lineTemplate = null;
        _elements = new ArrayList<>();
    }

    public Line(PageFormat pageFormat, LayoutConstraints layoutConstraints, LineTemplate lineTemplate) {
        this._group = 1;
        this._lineLength = (int) pageFormat.getImageableWidth();
        this._lineTemplate = lineTemplate;
        this._constraints = layoutConstraints;
        _elements = new ArrayList<>();
    }

    public void add(String line) {
        Pair<String, LayoutConstraints> pair = new Pair(line, this._constraints);
        this._elements.add(pair);
    }

    public void add(String line, LayoutConstraints layoutConstraints) throws IllegalArgumentException {
        if (layoutConstraints.wrap && layoutConstraints.maxwidth == 0 && this._lineTemplate == null) {
            System.out.println("" + layoutConstraints.maxwidth + " " + this._lineTemplate);
            throw new IllegalArgumentException("wrappable lines must have either a lineTemplate or a maxwidth constraint");
        } else {
            Pair<String, LayoutConstraints> pair = new Pair(line, layoutConstraints);
            this._elements.add(pair);
        }
    }

    public void setTemplate(LineTemplate lineTemplate) {
        this._lineTemplate = lineTemplate;
    }

    public void setConstraints(LayoutConstraints layoutConstraints) {
        this._constraints = layoutConstraints;
    }

    public Font getFont() {
        return this._constraints.font;
    }

    public static Line getLineSpacer(int var0) {
        return getLineSpacer(var0, new Font("SansSerif", 0, 10));
    }

    public static Line getLineSpacer(int spacer, Font font) {
        LayoutConstraints layoutConstraints = new LayoutConstraints();
        layoutConstraints.font = new Font(font.getName(), font.getStyle(), font.getSize() * spacer);
        return new Line(" ", layoutConstraints);
    }

    public void draw(Graphics2D graphics2D, int var2, int var3) {
        Iterator<Pair<String, LayoutConstraints>> iterator = this._elements.iterator();
        int lineIndex = 0;
        int var6 = var3;

        for (int var7 = 0; iterator.hasNext(); var3 = var6) {
            Pair<String, LayoutConstraints> pair = iterator.next();
            String line = pair.left();
            LayoutConstraints layoutConstraints = pair.right();
            graphics2D.setFont(layoutConstraints.font);
            int var11 = 0;
            if (layoutConstraints.fill == 1) {
                if (this._lineLength == 0) {
                    System.err.println("No page width, line can't be centered");
                } else {
                    var2 = this._lineLength / 2 - this.getLength(graphics2D) / 2;
                }
            }

            List<Pair<Integer, Pair<Integer, Column>>> columns;
            if (layoutConstraints.wrap) {
                if (this._lineTemplate != null) {
                    columns = this._lineTemplate.getColumns();
                    Pair<Integer, Pair<Integer, Column>> column = columns.get(lineIndex);
                    // var11 = (Integer) ((Pair) column.right()).right();
                    var11 = column.right().right().columnSize;
                    var7 = this._lineTemplate.getPad();
                    ++lineIndex;
                } else {
                    var11 = layoutConstraints.maxwidth;
                }

                FontRenderContext var17 = graphics2D.getFontRenderContext();
                AttributedCharacterIterator var19 = (new AttributedString(line)).getIterator();

                TextLayout var21;
                for (LineBreakMeasurer var20 = new LineBreakMeasurer(var19, var17); (var21 = var20.nextLayout((float) var11)) != null; var3 += graphics2D.getFontMetrics().getHeight()) {
                    var21.draw(graphics2D, (float) var2, (float) var3);
                }
            } else if (this._lineTemplate == null) {
                graphics2D.drawString(line + " ", var2, var3);
                var11 = graphics2D.getFontMetrics().stringWidth(line + " ");
                var7 = this._constraints.xpad;
            } else {
                ++lineIndex;
                columns = this._lineTemplate.getColumns();
                Iterator var13 = columns.iterator();
                var7 = layoutConstraints.xpad;

                while (var13.hasNext()) {
                    Pair var14 = (Pair) var13.next();
                    if ((Integer) var14.left() == lineIndex) {
                        Column var15 = (Column) ((Pair) var14.right()).right();
                        FontMetrics var16 = graphics2D.getFontMetrics();
                        var11 = var15.columnSize;
                        var11 += this._lineTemplate.getPad(this._group++, lineIndex);
                    }
                }

                graphics2D.drawString(line, var2, var3);
            }

            var2 += var11 + var7;
        }

    }

    public int getLength() {
        BufferedImage var1 = new BufferedImage(640, 480, 10);
        return this.getLength(var1.createGraphics());
    }

    public int getLength(Graphics2D var1) {
        FontMetrics var2 = var1.getFontMetrics(this._constraints.font);
        StringBuffer var3 = new StringBuffer(100);
        int var6 = 0;
        Iterator var4;
        Pair var7;
        if (this._lineTemplate == null) {
            var4 = this._elements.iterator();

            while (var4.hasNext()) {
                var7 = (Pair) var4.next();
                var3.append((String) var7.left());
                if (var4.hasNext()) {
                    var3.append(" ");
                }
            }

            var6 = var2.stringWidth(var3.toString());
        } else {
            for (var4 = this._lineTemplate.getColumns().iterator(); var4.hasNext(); var6 += (Integer) var7.right()) {
                var7 = (Pair) var4.next();
            }
        }

        return var6;
    }

    public int getHeight() {
        BufferedImage var1 = new BufferedImage(640, 480, 10);
        Graphics2D var2 = var1.createGraphics();
        return this.getHeight(var2);
    }

    public int getHeight(Graphics2D var1) {
        FontRenderContext var2 = var1.getFontRenderContext();
        Iterator var3 = this._elements.iterator();
        int var5 = 0;

        for (int var6 = 1; var3.hasNext(); ++var6) {
            Pair var7 = (Pair) var3.next();
            String var8 = (String) var7.left();
            LayoutConstraints var9 = (LayoutConstraints) var7.right();
            Font var10 = var9.font;
            int var11 = 0;
            var1.setFont(var10);
            int var12 = var1.getFontMetrics().getHeight();
            if (!var9.wrap) {
                if (var12 > var5) {
                    var5 = var12;
                }
            } else {
                if (var9.maxwidth != 0) {
                    var11 = var9.maxwidth;
                } else {
                    List<Pair<Integer, Pair<Integer, Column>>> columns = this._lineTemplate.getColumns();
                    Iterator<Pair<Integer, Pair<Integer, Column>>> var14 = columns.iterator();
                    while (var14.hasNext()) {
                        Pair var15 = (Pair) var14.next();
                        if ((Integer) var15.left() == var6) {
                            var11 = (Integer) var15.right();
                        }
                    }
                }

                AttributedCharacterIterator var16 = (new AttributedString(var8)).getIterator();

                for (LineBreakMeasurer var17 = new LineBreakMeasurer(var16, var2); var17.nextLayout((float) var11) != null; var5 += var12) {
                }
            }
        }

        return var5;
    }

}
