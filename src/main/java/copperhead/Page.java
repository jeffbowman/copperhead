package copperhead;

import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.util.ArrayList;
import java.util.Iterator;

public class Page implements Printable {
    private ArrayList _lines = new ArrayList();
    private int _height;
    private int _width;
    private int _availableHeight;

    public Page(int var1, int var2) {
        this._height = var2;
        this._width = var1;
        this._availableHeight = var2;
    }

    public boolean addLine(Line var1) {
        boolean var2 = false;
        if (var1.getHeight() <= this._availableHeight) {
            this._lines.add(var1);
            var2 = true;
            this._availableHeight -= var1.getHeight();
        } else {
            this._availableHeight = this._height;
        }

        return var2;
    }

    public int print(Graphics var1, PageFormat var2, int var3) {
        System.out.println("Printing Page..." + var3);
        Graphics2D var4 = (Graphics2D) var1;
        Iterator var5 = this._lines.iterator();
        int var6 = (int) var2.getImageableX();
        int var7 = (int) var2.getImageableY();
        int var8 = ((Line) this._lines.get(0)).getHeight();

        Line var9;
        for (var7 += var8; var5.hasNext(); var7 += var9.getHeight()) {
            var9 = (Line) var5.next();
            var9.draw(var4, var6, var7);
            int var10 = var9.getHeight();
        }

        return 0;
    }

}
