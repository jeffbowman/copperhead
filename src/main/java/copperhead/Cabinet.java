package copperhead;

import java.util.Collection;
import java.util.Map;

public interface Cabinet {
    Dim bulkhead();

    Dim3D dimension();

    void dimension(Dim3D var1);

    Collection<Map<String, String>> fields();

    Dim mullions();

    int numMullions();

    Dim opening();

    Dim rails();

    int numRails();

    Dim shelves();

    Dim stiles();

    int numStiles();

    Map<String, String> showBulkheads();

    Map<String, String> showDims();

    Map<String, String> showMullions();

    Map<String, String> showOpening();

    Map<String, String> showRails();

    Map<String, String> showShelves();

    Map<String, String> showStiles();

    String getLenStr();

    String getWidStr();

    String getHgtStr();

    String id();

    void id(String var1);

}
