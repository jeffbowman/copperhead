package copperhead;

import java.util.*;

public class BaseCabinet extends AbstractCabinet {
    private Dim _baseDim;
    private Dim _drawerOpening;
    private Dim _face;

    public BaseCabinet() {
        this._drawerOpening = null;
        this._face = null;
        this._baseDim = null;
        this._dimension = new Dim3D();
    }

    public BaseCabinet(Dim3D dimension) {
        _dimension = dimension;
        Fraction height = _dimension.height();
        _face = new Dim(height.subtract(new Fraction(13, 4)), _dimension.width());
        _drawerOpening = null;
        _baseDim = null;
    }

    public Dim baseDim() {
        Fraction faceLength = this._face.length();
        Fraction two = new Fraction(2, 1);

        Fraction railsLength = this.rails().length();

        Fraction drawerOpeningWidth = this.drawerOpening().width();

        Fraction bottomRail = this.bottomRail().length();

        faceLength = faceLength.subtract(railsLength.multiply(two))
                .subtract(drawerOpeningWidth)
                .subtract(bottomRail);

        _baseDim = new Dim(faceLength, bottomRail);

        return _baseDim;
    }

    public Dim bottomRail() {
        Fraction length = new Fraction(3, 2);
        Fraction stilesLen = stiles().length();
        Fraction numStiles = new Fraction(numStiles());
        Fraction railLength = _dimension.length();
        Fraction width = railLength.subtract(numStiles.multiply(stilesLen));
        return new Dim(length, width);
    }

    public Dim bulkhead() {
        Number var1 = this._dimension.width();
        Fraction var2 = var1 instanceof Fraction ? (Fraction) var1 : new Fraction(var1.toString());
        var2 = var2.subtract(new Fraction(1, 2));
        var1 = this._dimension.height();
        Fraction var3 = var1 instanceof Fraction ? (Fraction) var1 : new Fraction(var1.toString());
        Dim var4 = new Dim(var2, var3);
        return var4;
    }

    public void dimension(Dim3D dim3D) {
        super.dimension(dim3D);
        Number var2 = this._dimension.height();
        Fraction var3 = var2 instanceof Fraction ? (Fraction) var2 : new Fraction(var2.toString());
        var3 = var3.subtract(new Fraction(13, 4));
        var2 = this._dimension.width();
        Fraction var4 = var2 instanceof Fraction ? (Fraction) var2 : new Fraction(var2.toString());
        this._face = new Dim(var3, var4);
    }

    public Dim drawerOpening() {
        Number var1 = this._dimension.width();
        Fraction var2 = var1 instanceof Fraction ? (Fraction) var1 : new Fraction(var1.toString());
        Fraction var3 = new Fraction((new Integer(this.numStiles())).toString());
        var1 = this.stiles().length();
        Fraction var4 = var1 instanceof Fraction ? (Fraction) var1 : new Fraction(var1.toString());
        var2 = var2.subtract(var3.multiply(var4));
        Fraction var5 = new Fraction(5, 1);
        Dim var6 = new Dim(var2, var5);
        return var6;
    }

    public Dim face() {
        return this._face;
    }

    public void face(Dim var1) {
        this._face = var1;
    }

    public Collection<Map<String, String>> fields() {
        List<Map<String, String>> var1 = new ArrayList<>();
        Map<String, String> var2 = new HashMap<>();
        Map<String, String> var3 = new HashMap();
        var2.put("Type: ", "BASE " + this.id());
        var3.put("Dims: ", this._dimension.toString());
        var1.add(var2);
        var1.add(var3);
        var1.add(showStiles());
        var1.add(showRails());
        var1.add(showBottomRail());
        var1.add(showMullions());
        var1.add(showOpening());
        var1.add(showShelves());
        var1.add(showBulkheads());
        return var1;
    }

    public Dim mullions() {
        Fraction var1 = new Fraction(2, 1);
        Number var2 = this.baseDim().length();
        Fraction var3 = var2 instanceof Fraction ? (Fraction) var2 : new Fraction(var2.toString());
        Dim var4 = new Dim(var1, var3);
        return var4;
    }

    public int numRails() {
        return 3;
    }

    public Dim opening() {
        Fraction var1 = null;
        Number var2 = this._dimension.length();
        Fraction var3 = var2 instanceof Fraction ? (Fraction) var2 : new Fraction(var2.toString());
        var2 = this.stiles().length();
        Fraction var4 = var2 instanceof Fraction ? (Fraction) var2 : new Fraction(var2.toString());
        Fraction var5 = new Fraction((new Integer(this.numStiles())).toString());
        var3 = var3.subtract(var5.multiply(var4));
        if (var3.greaterThan(new Fraction(20, 1))) {
            Fraction var6 = new Fraction(this.numMullions(), 1);
            Fraction var7 = var6.divide(new Fraction(2, 1));
            var1 = var3.subtract(var6);
            var1 = var1.divide(var7.add(new Fraction(1, 1)));
        } else {
            var1 = var3;
        }

        var2 = this.baseDim().length();
        var3 = var2 instanceof Fraction ? (Fraction) var2 : new Fraction(var2.toString());
        Dim var8 = new Dim(var1, var3);
        return var8;
    }

    public String toString() {
        return this.id() + ":BASE: " + this._dimension.toString();
    }

    public Dim shelves() {
        Number var1 = this._dimension.length();
        Fraction var2 = var1 instanceof Fraction ? (Fraction) var1 : new Fraction(var1.toString());
        var2 = var2.subtract(new Fraction(1, 1));
        var1 = this._dimension.width();
        Fraction var3 = var1 instanceof Fraction ? (Fraction) var1 : new Fraction(var1.toString());
        var3 = var3.subtract(new Fraction(3, 4));
        Dim var4 = new Dim(var2, var3);
        return var4;
    }

    public Map<String, String> showBottomRail() {
        Map<String, String> var1 = new HashMap<>();
        var1.put("Bottom Rail: ", "1 @ " + this.bottomRail().toString());
        return var1;
    }

    public Map<String, String> showBulkheads() {
        Map<String, String> var1 = new HashMap<>();
        var1.put("Bulkhead: ", this.bulkhead().toString());
        return var1;
    }

    public Map<String, String> showDims() {
        Map<String, String> var1 = new HashMap<>();
        var1.put("Dims: ", this.dimension().toString());
        return var1;
    }

    public Map<String, String> showMullions() {
        Map<String, String> var1 = new HashMap<>();
        String var2 = this.mullions().toString() + " & " + this.numMullions() / 2;
        Fraction var3 = this.mullions().length();
        var2 = var2 + " @ " + var3.toMixedString();
        var2 = var2 + "x" + this.drawerOpening().width().toString();
        var1.put("Mullions: ", "" + this.numMullions() / 2 + " @ " + var2);
        return var1;
    }

    public Map<String, String> showOpening() {
        Map<String, String> var1 = new HashMap<>();
        int var2 = this.numMullions() / 2 + 1;
        var1.put("Opening: ", "" + var2 + "  @ " + this.opening().toString());
        return var1;
    }

    public Map<String, String> showRails() {
        Map<String, String> var1 = new HashMap<>();
        int var2 = this.numRails() - 1;
        var1.put("Rails: ", "" + var2 + " @ " + this.rails().toString());
        return var1;
    }

    public Map<String, String> showShelves() {
        Map<String, String> var1 = new HashMap<>();
        var1.put("Shelves: ", this.shelves().toString());
        return var1;
    }

    public Map<String, String> showStiles() {
        Map<String, String> var1 = new HashMap<>();
        int var2 = this.numStiles();
        var1.put("Stiles: ", "" + var2 + " @ " + this.stiles().toString());
        return var1;
    }

    public Dim stiles() {
        Fraction var1 = new Fraction(3, 2);
        Number var2 = this.face().length();
        Fraction var3 = var2 instanceof Fraction ? (Fraction) var2 : new Fraction(var2.toString());
        Dim var4 = new Dim(var1, var3);
        return var4;
    }

    public String id() {
        return super.id();
    }

    public void id(String var1) {
        super.id(var1);
    }
}
