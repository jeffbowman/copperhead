package copperhead;

public class Dim3D extends Dim {

    public Dim3D() {
        this._length = new Fraction(0, 1);
        this._width = new Fraction(0, 1);
        this._height = new Fraction(0, 1);
    }

    public Dim3D(Number length, Number width, Number height) {
        this._length = Fraction.asFraction(length);
        this._width = Fraction.asFraction(width);
        this._height = Fraction.asFraction(height);
    }

    public Fraction height() {
        return this._height;
    }

    public String toString() {
        if (str != null && str.length() > 0 && str.contains("x")) {
            return str;
        }
        StringBuilder builder = new StringBuilder(super.toString());
        builder.append("x").append(_height.toMixedString());
        str = builder.toString();
        return str;
    }

    protected Fraction _height;
    private String str;
}
